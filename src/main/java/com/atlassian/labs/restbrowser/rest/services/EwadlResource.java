package com.atlassian.labs.restbrowser.rest.services;

import com.atlassian.annotations.security.AnonymousSiteAccess;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Provides the ewadl resource.
 */
@Path("ewadl")
public class EwadlResource {

    // This WADL data describes Jira 5, so it doesn't seem very useful, especially in other products
    private static final String EWADL_TEMPLATE = "/templates/ewadl.vm";

    private final TemplateRenderer renderer;

    @Inject
    public EwadlResource(TemplateRenderer renderer) {
        this.renderer = renderer;
    }

    @AnonymousSiteAccess
    @AnonymousAllowed
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getWadl() throws IOException {
        StringWriter xmlContainer = new StringWriter();
        renderer.render(EWADL_TEMPLATE, xmlContainer);
        return Response.ok(xmlContainer.toString()).build();
    }

}
